﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_5._4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите количество строк: "); int n = Convert.ToInt32(Console.ReadLine());
            Console.Write("Введите количество столбцов: "); int m = Convert.ToInt32(Console.ReadLine());
            int[,] a = new int[n,m];
            gen(a, n, m);
            write(a, m);
            min(a, n, m);
            double sred = sr(a, n, m);
            Console.WriteLine("Среднее арифметическое: {0}", sred);
            Console.ReadLine();
        }
        static void write(int[,] arr, int m)
        {
            int k = 0;
            foreach (int elem in arr)
            {
                Console.Write("{0,3} ", elem);
                ++k;
                if (k == m)
                {
                    Console.WriteLine();
                    k = 0;
                }
            } 
        }
        static void gen(int[,] a, int n, int m)
        {
            Random x = new Random();
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < m; ++j)
                {
                    a[i, j] = x.Next(1, 100);
                }

            }
        }
        static void min(int[,] a, int n, int m)
        {
            for (int i = 0; i < n; ++i)
            {
                int minimum = a[i, 0];
                for (int j = 0; j < m; ++j)
                {
                    if (a[i, j] < minimum) minimum = a[i, j];
                }
                Console.WriteLine("Минимальное число в строке {0}: {1}", i+1, minimum);
            }
        }
        static double sr(int[,] a, int n, int m)
        {
            int min = a[0, 0];
            int max = a[0, 0];
            foreach (int elem in a)
            {
                if (elem < min) min = elem;
                if (elem > max) max = elem;
            }
            Console.WriteLine("Минимальное число в массиве: {0}", min);
            Console.WriteLine("Макимальное число в массиве: {0}", max);
            int sum = 0;
            int ink = 0;
            foreach (int elem in a)
            {
                if (elem != max && elem != min)
                {
                    sum += elem;
                    ++ink;
                }
            }
            double sr = sum / ink;
            return sr;
        }
    }
}

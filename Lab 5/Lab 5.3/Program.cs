﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_5._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Random x = new Random();
            Console.Write("Введите размерность массива A: "); int n = Convert.ToInt32(Console.ReadLine());
            int[] a = new int[n];
            Console.Write("Введите размерность массива B: "); int m = Convert.ToInt32(Console.ReadLine());
            int[] b = new int[m];
            int[] c = new int[n + m];
            a[0] = x.Next(0, 10);
            b[0] = x.Next(0, 10);
            for (int i = 1; i < n; ++i)
            {
                a[i] = x.Next(a[i - 1] + 1, a[i - 1] + x.Next(1, 5));
            }
            for (int i = 1; i < m; ++i)
            {
                b[i] = x.Next(b[i - 1] + 1, b[i - 1] + x.Next(1, 5));
            }
            Sortirovka(a, b, c);
            Console.WriteLine("Отсортированный массив C: ");
            for (int i = 0; i < n+m; ++i)
            {
                Console.Write("{0} ", c[i]);
            }
            Console.ReadLine();
        }

        static void Sortirovka(int[] a, int[] b, int[] res)
        {
            int pos = 0;
            int l = 0;
            int r = 0;
            for (; l < a.Length && r < b.Length; )
            {
                if (a[l] == b[r])
                {
                    res[pos++] = a[l++];
                    res[pos++] = b[r++];
                }
                else
                    if (a[l] < b[r])
                    {
                        res[pos++] = a[l++];
                    }
                    else
                        res[pos++] = b[r++];
            }
            if (l == a.Length)
                for (; r < b.Length; )
                    res[pos++] = b[r++];
            else
                for (; l < a.Length; )
                    res[pos++] = a[l++];
        }
    }
}

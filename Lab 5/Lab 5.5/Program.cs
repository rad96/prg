using System;
using System.Collections.Generic;
using System.Text;
namespace Lab_5._5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Введите количество строк: "); int n = Convert.ToInt32(Console.ReadLine());
            Console.Write("Введите количество столбцов: "); int m = Convert.ToInt32(Console.ReadLine());
            double[,] a = new double[n, m];
            gendouble(a, n, m);
            write(a, m);
            Console.WriteLine();
            Console.WriteLine("Локальные минимумы в матрице:");
            int test = localminimum(a, n, m);
            Console.WriteLine();
            Console.WriteLine("Всего локальных минимумов в матрице: {0}", test);
            Console.ReadLine();
        }
        static int localminimum(double[,] a, int n, int m)
        {
            int ink = 0;
            for (int i = 0; i < m; ++i) //Верхняя строка
            {
                if (i == 0)
                {
                    if (a[0, i] < a[0, i + 1] && a[0, i] < a[1, i])
                    {
                        ++ink;
                        Console.WriteLine(a[0, i]);
                    }
                }
                else
                {
                    if (i == m - 1)
                    {
                        if (a[0, i] < a[0, i - 1] && a[0, i] < a[1, i])
                        {
                            ++ink;
                            Console.WriteLine(a[0, i]);
                        }
                    }
                    else
                    {
                        if (a[0, i] < a[0, i - 1] && a[0, i] < a[1, i] && a[0, i] < a[0, i + 1])
                        {
                            ++ink;
                            Console.WriteLine(a[0, i]);
                        }
                    }
                }
            }
            for (int i = 1; i < n - 1; ++i) //Левый столбец
            {
                if (a[i, 0] < a[i + 1, 0] && a[i, 0] < a[i, 1] && a[i, 0] < a[i - 1, 0])
                {
                    ++ink;
                    Console.WriteLine(a[i, 0]);
                }

            }
            for (int i = 1; i < n - 1; ++i) //Центр
            {
                for (int j = 1; j < m - 1; ++j)
                {
                    if (a[i, j] < a[i + 1, j] && a[i, j] < a[i - 1, j] && a[i, j] < a[i, j + 1] && a[i, j] < a[i, j - 1])
                    {
                        ++ink;
                        Console.WriteLine(a[i, j]);
                    }
                }
            }
            for (int i = 1; i < n - 1; ++i) //Правый столбец
            {
                if (a[i, m - 1] < a[i + 1, m - 1] && a[i, m - 1] < a[i, m - 2] && a[i, m - 1] < a[i - 1, m - 1])
                {
                    ++ink;
                    Console.WriteLine(a[i, m - 1]);
                }

            }
            for (int i = 0; i < m; ++i) //Нижняя строка
            {
                if (i == 0)
                {
                    if (a[n - 1, i] < a[n - 1, i + 1] && a[n - 1, i] < a[n - 2, i])
                    {
                        ++ink;
                        Console.WriteLine(a[n - 1, i]);
                    }
                }
                else
                {
                    if (i == m - 1)
                    {
                        if (a[n - 1, i] < a[n - 1, i - 1] && a[n - 1, i] < a[n - 2, i])
                        {
                            ++ink;
                            Console.WriteLine(a[n - 1, i]);
                        }
                    }
                    else
                    {
                        if (a[n - 1, i] < a[n - 1, i - 1] && a[n - 1, i] < a[n - 2, i] && a[n - 1, i] < a[n - 1, i + 1])
                        {
                            ++ink;
                            Console.WriteLine(a[n - 1, i]);
                        }
                    }
                }
            }
            return ink;
        }
        static void write(double[,] arr, int m)
        {
            int k = 0;
            foreach (double elem in arr)
            {
                Console.Write("{0:00.00} ", elem);
                ++k;
                if (k == m)
                {
                    Console.WriteLine();
                    k = 0;
                }
            }
        }
        static void gendouble(double[,] a, int n, int m)
        {
            Random x = new Random();
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < m; ++j)
                {
                    a[i, j] = x.Next(1, 100) + (x.Next(1, 100) / 100.0);
                }
            }
        }
    }
}

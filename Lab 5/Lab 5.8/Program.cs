﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_5._8
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][,] a = new int[][,] 
            {
                new int[2,2],
                new int[3,2],
                new int[2,3] 
            };
            int sum = 0;
            Console.WriteLine("Первый массив");
            genarr(a[0], 2, 2);
            write(a[0], 2);

            sum = sum + max(a[0], 2, 2);
            Console.WriteLine("Второй массив");
            genarr(a[1], 3, 2);
            write(a[1], 2);

            sum = sum + max(a[1], 3, 2);
            Console.WriteLine("Третий массив");
            genarr(a[2], 2, 3);
            write(a[2], 3);

            sum = sum + max(a[2], 2, 3);
            Console.WriteLine("Сумма всех максимальных чисел: {0}", sum);
            Console.ReadLine();
        }
        static int max(int[,] a, int n, int m)
        {
            int sum = 0;
            for (int j = 0; j < m; j++)
            {
                int max = a[0, j];
                for (int i = 0; i < n; i++)
                {
                    if (a[i, j] >= max) max = a[i, j];
                }
                sum = sum + max;
            }
            return sum;
        }
        static void genarr(int[,] a, int n, int m)
        {
            Random x = new Random();
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < m; ++j)
                {
                    a[i, j] = x.Next(1, 100);
                }

            }
        }
        static void write(int[,] arr, int m)
        {
            int k = 0;
            foreach (int elem in arr)
            {
                Console.Write("{0,3} ", elem);
                ++k;
                if (k == m)
                {
                    Console.WriteLine();
                    k = 0;
                }
            }
        }
    }
}

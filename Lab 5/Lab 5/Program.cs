﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_5
{
    class Program
    {
        static void Main(string[] args)
        {       
            Random x = new Random();
            int[] arr = new int[15];
            for (int i = 0; i <= 14; i++)
            {
                arr[i] = Convert.ToInt32(x.Next(-100, 100));
                Console.WriteLine("{0}-ый элемент массива: {1}",i+1, arr[i]);
            }
            int osum = minus(arr);
            Console.WriteLine("Количество отрицательных чисел в массиве: {0}", osum);
            int chetsum = chet(arr);
            Console.WriteLine("Количество четных чисел в массиве: {0}", chetsum);
            int maxnum = max(arr);
            Console.WriteLine("Максимальное число в массиве: {0}", maxnum);
            Console.WriteLine();
            chetelem(arr);
            Console.ReadLine();
        }
        static int minus(int[] array)
        {
            int x = 0;
            for (int n = 0; n <= 14; n++)
            {
                if (array[n] < 0) x = x + 1;
            }
            return x;
        }
        static int chet(int[] array)
        {
            int x = 0;
            for (int n = 0; n <= 14; n++)
            {
                if (array[n] % 2 == 0 && array[n] != 0) x = x + 1;
            }
            return x;
        }
        static int max(int[] array)
        {
            int x = 0;
            for (int n = 0; n <= 14; n++)
            {
                if (array[n] > x) x = array[n];
            }
            return x;
        }
        static void chetelem(int[] array)
        {
            for (int n = 1; n <= 14; n++)
            {
                if ((n-1) % 2 == 0) Console.WriteLine("{0}-ый элемент массива: {1}", n + 1, array[n]); ;
            }
        }
    }
}

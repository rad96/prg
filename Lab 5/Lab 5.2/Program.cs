﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_5._2
{
    class Program
    {
        static void Main(string[] args)
        {
            Random x = new Random();
            Console.Write("Введите размер массива: "); int n = Convert.ToInt32(Console.ReadLine());
            int[] a = new int[n];
            for (int i = 0; i < n; ++i)
            {
                a[i] = Convert.ToInt32(x.Next(0, 99));
            }
            Console.WriteLine("Исходный массив:");
            for (int i = 0; i < n; ++i)
                Console.Write("{0} ", a[i]);
            Console.WriteLine();
            Console.Write("Введите смещение: "); int k = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < k; ++i)
            {
                int aLast = a[n - 1];
                for (int j = n - 1; j > 0; j--)
                    a[j] = a[j - 1];
                a[0] = aLast;
            }
            Console.WriteLine("Смещенный массив: ");
            for (int i = 0; i < n; ++i)
                Console.Write("{0} ", a[i]);
            Console.WriteLine();
            Console.ReadKey();
        }
    }
}

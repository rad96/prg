﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_5._7
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            Random x = new Random();
            int swt = 0;
            Console.Write("Введите длинну строки: "); int l = Convert.ToInt32(Console.ReadLine());
            StringBuilder s = new StringBuilder();
            s.Length = l;
            for (int i = 0; i < l; ++i)
            {
                swt = x.Next(1, 4);
                if (swt == 1) s[i] = Convert.ToChar(rand.Next(0x0061, 0x0068));
                if (swt == 2) s[i] = Convert.ToChar(rand.Next(0x0030, 0x0033));
                if (swt == 3)
                {
                    swt = x.Next(1, 4);
                    if (swt == 1) s[i] = '?';
                    if (swt == 2) s[i] = '!';
                    if (swt == 3) s[i] = ';';
                }
            }
            Console.WriteLine("Исходная строка:");
            Console.WriteLine(s);
            for (int i = 0; i < l; ++i)
            {
                if (s[i] == ';') s[i] = '_';
            }
            Console.WriteLine("Измененная строка:");
            Console.WriteLine(s);
            Console.ReadLine();
        }
    }
}

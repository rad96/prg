﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3._3
{
    class Program
    {
        static void Main(string[] args)
        {
            double sum = 1;
            double x = 0;
            Console.Write("Введите длину ряда для вычислений: ");
            int n = Convert.ToInt32(Console.ReadLine());
            for(int i=2; i <=n; i++)
            {
                double f = 1;
                for (int c = i; c > 1; c--)
                {
                    f = f * c;
                }
                x = 1 / f;
                sum = sum + x;
                Console.WriteLine("Факториал составил: {0}, Сумма на итерации {1} составляет {2}", f, i, sum);
            }
            Console.ReadLine();
        }
    }
}

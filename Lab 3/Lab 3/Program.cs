﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int f = 0;
            Console.Write("Введите x: ");
            int x = Convert.ToInt32(Console.ReadLine());
            Console.Write("Введите a: ");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.Write("Введите b: ");
            int b = Convert.ToInt32(Console.ReadLine());
            Console.Write("Введите c: ");
            int c = Convert.ToInt32(Console.ReadLine());
            if (c < 0 && x != 0) f = -a * x - c;
            else
            {
                if (c > 0 && b == 0) f = (x - a) / -c;
                else { f = b * x / (c - a); }
            }
            Console.Write("F={0}", f);
            Console.ReadLine();
        }
    }
}

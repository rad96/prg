﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3._2
{
    class Program
    {
        static void Main(string[] args)
        {
            double sum = 0;
            int check = 0;
            Console.Write("Введите длину ряда для вычислений: ");
            int n = Convert.ToInt32(Console.ReadLine());
            for (int k=0; k<n; k++)
            {
                double x = Math.Pow(-1, k) / (2 * k + 1);
                sum = sum + x;
                ++check;
                if (check==4)
                {
                    sum = sum - x;
                    check = 0;
                    Console.WriteLine("Встречен четвертый элемент");
                }
                else
                {
                    Console.WriteLine("K={0}, sum={1}", k, sum);
                }
            }
            Console.ReadLine();
        }
    }
}

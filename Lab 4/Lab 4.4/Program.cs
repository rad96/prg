using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Lab_4._4
{
    class Program
    {
        static void Main(string[] args)
        {
            double f;
            long time1, time2;
            double n, a, res;
            Console.Write("Введите число:"); a = Convert.ToDouble(Console.ReadLine());
            Console.Write("Введите необходимую степень:"); n = Convert.ToDouble(Console.ReadLine());
            res = Pow(a, n);
            Console.WriteLine("Число {0}, в степени {1} равно: {2}", a, n, res);
            time1 = getTimeInMilliseconds();
            for (int i = 1; i < 1000000; i++) f = Pow(a, n);
            time2 = getTimeInMilliseconds();
            Console.WriteLine("Время работы процедуры: {0}", time2 - time1);
            Console.ReadLine();
        }
        static double Pow(double value, double pow)
        {
            if (pow > 0) return value * Pow(value, pow - 1);
            else if (pow == 0) return 1;
            else return 1 / (value * Pow(value, -pow - 1));
        }
        static long getTimeInMilliseconds()
        {
            DateTime time = DateTime.Now;
            return (((time.Hour * 60 + time.Minute) * 60 + time.Second) * 1000 + time.Millisecond);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_4
{
    class Program
    {
        static void Main(string[] args)
        {
            int result;
            Console.Write("Введите число:"); int x = Convert.ToInt32(Console.ReadLine());
            result = NumberOfZeroes(x);
            Console.WriteLine(result); Console.ReadLine();
        }
        static int NumberOfZeroes(int n)
        {
            int i = 0;
            while (n > 0)
            {
                if (n % 10 == 0) ++i;
                n /= 10;
            }
            return i;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_4._2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 1;
            int sum = 0;
            while (n != 0)
            {
                try
                {
                    Console.Write("Введите число: "); n = Convert.ToInt32(Console.ReadLine());
                }
                catch (FormatException)
                {
                    Console.Write("Вы ввели слово или пустую строку, пожалуйста, повторите ввод: "); n = Convert.ToInt32(Console.ReadLine());
                }
                sum = sum + n;
                Console.WriteLine("Текущая сумма: {0}", sum);
            }
            Console.WriteLine("Вы ввели 0"); Console.WriteLine("Окончательная сумма: {0}", sum); Console.ReadLine();
        }
    }
}

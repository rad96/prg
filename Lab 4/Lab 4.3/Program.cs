using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Lab_4._3
{
    class Program
    {
        static void Main(string[] args)
        {
            int f;
            long time1, time2;
            Console.Write("Введите число:");
            int x = Convert.ToInt32(Console.ReadLine());
            x = cut(x);
            Console.WriteLine("Обновленное число: {0}", x);            
            time1 = getTimeInMilliseconds();
            for (int i = 1; i < 1000000; i++) f = cut(x);
            time2 = getTimeInMilliseconds();
            Console.WriteLine("Время работы процедуры: {0}", time2 - time1);
            Console.ReadLine();
        }
        static int cut(int s) 
        {
            int ish = s;
            s /= 10;
            int chisl = 0;
            int last = 0;
            while (s != 0)
            {
                ++chisl;
                last = s;
                s /= 10;
            }
            int minus = 1;
            for (int i = 0; i < chisl; ++i) 
            {
                minus = minus * 10;
            }
            minus = last * minus;
            s = ish - minus;
            s /= 10;
            return s;
        }
        static long getTimeInMilliseconds()
        {
            DateTime time = DateTime.Now;
            return (((time.Hour * 60 + time.Minute) * 60 + time.Second) * 1000 + time.Millisecond);
        }
    }
}

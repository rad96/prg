﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_8._2
{
    class Storage
    {
        Items[] array = new Items[3];
        public Storage()
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = new Items();
            }
        }
        public string[,] GetDataTable()
        {
            string[,] Table = new string[array.Length, 4];
            for (int i = 0; i < Table.GetLength(0); i++)
            {
                Table[i, 0] = Convert.ToString(array[i].GetIndex());
                Table[i, 1] = array[i].GetName();
                Table[i, 2] = Convert.ToString(array[i].GetAmount());
                Table[i, 3] = Convert.ToString(array[i].GetCost());
            }
            return Table;
        }
        public Items[] GetArray()
        {
            return this.array;
        }
        public void EditItem(int index, string[] inf)
        {
            array[index - 1].ChangeItem(inf);
        }
        public string[] GetItem(int index)
        {
            string[] info = new string[4];
            try
            {
                info[0] = array[index - 1].GetName();
                info[1] = Convert.ToString(array[index - 1].GetAmount());
                info[2] = Convert.ToString(array[index - 1].GetCost());
            }
            catch (IndexOutOfRangeException)
            {
                MessageBox.Show("Ошибка, индекс выходит за пределы массива", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
            return info;
        }
    }
    class Items
    {
        private int indexnum;
        private string name;
        private int amount;
        private double cost;
        public void ChangeItem(string[] InfFromForm)
        {
            this.name = InfFromForm[0];
            this.amount = Convert.ToInt32(InfFromForm[1]);
            this.cost = Convert.ToDouble(InfFromForm[2]);
            this.indexnum = Convert.ToInt32(InfFromForm[3]);
        }
        public string GetName()
        {
            return this.name;
        }
        public int GetAmount()
        {
            return this.amount;
        }
        public double GetCost()
        {
            return this.cost;
        }
        public int GetIndex()
        {
            return this.indexnum;
        }
    }
}

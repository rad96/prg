﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_8._2
{
    public partial class MainForm : Form
    {
        Storage store = new Storage();
        public MainForm()
        {
            InitializeComponent();
            grid.ColumnCount = 4;
            grid.RowCount = 1;
            grid.Columns[0].HeaderText = "Индекс";
            grid.Columns[1].HeaderText = "Имя";
            grid.Columns[2].HeaderText = "Количество";
            grid.Columns[3].HeaderText = "Цена";
        }
        private void ChangeItemButton_Click(object sender, EventArgs e)
        {
            try
            {
                string[] info = { NameBox.Text, AmountBox.Text, CostBox.Text, IndexBox.Text };
                store.EditItem(Convert.ToInt32(IndexBox.Text), info);
            }
            catch (FormatException)
            {
                MessageBox.Show("Ошибка формата, либо одно из полей пустое", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }
        private void InfoButton_Click(object sender, EventArgs e)
        {
            string[] GottenItem = store.GetItem(Convert.ToInt32(IndexBox.Text));
            NameBox.Text = GottenItem[0];
            AmountBox.Text = GottenItem[1];
            CostBox.Text = GottenItem[2];
        }
        private void NameSearchButton_Click(object sender, EventArgs e)
        {
            int count = 0;
            Items[] array = store.GetArray();
            foreach (Items elem in array)
            {
                if (string.Compare(elem.GetName(), NameBox.Text) == 0)
                {
                    AmountBox.Text = Convert.ToString(elem.GetAmount());
                    CostBox.Text = Convert.ToString(elem.GetCost());
                    IndexBox.Text = Convert.ToString(elem.GetIndex());
                    break;
                }
                count++;
            }
            if (count == array.Length)
            {
                MessageBox.Show("Данного товара не обнаружено", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }
        private void GenTableButton_Click(object sender, EventArgs e)
        {
            string[,] Table = store.GetDataTable();
            grid.RowCount = Table.GetLength(0)+1;
            for (int i = 0; i < Table.GetLength(0); i++)
            {
                grid[0, i].Value = Table[i, 0];
                grid[1, i].Value = Table[i, 1];
                grid[2, i].Value = Table[i, 2];
                grid[3, i].Value = Table[i, 3];
            }
        }

        private void NameSortA_Click(object sender, EventArgs e)
        {
            DataGridViewColumn column = grid.Columns[1];
            grid.Sort(column, ListSortDirection.Ascending);
        }

        private void AmountSortA_Click(object sender, EventArgs e)
        {
            DataGridViewColumn column = grid.Columns[2];
            grid.Sort(column, ListSortDirection.Ascending);
        }

        private void CostSortA_Click(object sender, EventArgs e)
        {
            DataGridViewColumn column = grid.Columns[3];
            grid.Sort(column, ListSortDirection.Ascending);
        }

        private void NameSortB_Click(object sender, EventArgs e)
        {
            DataGridViewColumn column = grid.Columns[1];
            grid.Sort(column, ListSortDirection.Descending);
        }

        private void AmountSortD_Click(object sender, EventArgs e)
        {
            DataGridViewColumn column = grid.Columns[2];
            grid.Sort(column, ListSortDirection.Descending);
        }

        private void CostSortD_Click(object sender, EventArgs e)
        {
            DataGridViewColumn column = grid.Columns[3];
            grid.Sort(column, ListSortDirection.Descending);
        }
    }
}

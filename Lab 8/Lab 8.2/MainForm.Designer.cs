﻿namespace Lab_8._2
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChangeItemButton = new System.Windows.Forms.Button();
            this.InfoButton = new System.Windows.Forms.Button();
            this.IndexBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.NameBox = new System.Windows.Forms.TextBox();
            this.CostBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.AmountBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.NameSearchButton = new System.Windows.Forms.Button();
            this.grid = new System.Windows.Forms.DataGridView();
            this.GenTableButton = new System.Windows.Forms.Button();
            this.AmountSortA = new System.Windows.Forms.Button();
            this.NameSortA = new System.Windows.Forms.Button();
            this.CostSortA = new System.Windows.Forms.Button();
            this.NameSortB = new System.Windows.Forms.Button();
            this.AmountSortD = new System.Windows.Forms.Button();
            this.CostSortD = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.grid)).BeginInit();
            this.SuspendLayout();
            // 
            // ChangeItemButton
            // 
            this.ChangeItemButton.Location = new System.Drawing.Point(31, 80);
            this.ChangeItemButton.Name = "ChangeItemButton";
            this.ChangeItemButton.Size = new System.Drawing.Size(147, 45);
            this.ChangeItemButton.TabIndex = 0;
            this.ChangeItemButton.Text = "Изменить товар";
            this.ChangeItemButton.UseVisualStyleBackColor = true;
            this.ChangeItemButton.Click += new System.EventHandler(this.ChangeItemButton_Click);
            // 
            // InfoButton
            // 
            this.InfoButton.Location = new System.Drawing.Point(184, 82);
            this.InfoButton.Name = "InfoButton";
            this.InfoButton.Size = new System.Drawing.Size(147, 43);
            this.InfoButton.TabIndex = 1;
            this.InfoButton.Text = "Получить информацию по индексу";
            this.InfoButton.UseVisualStyleBackColor = true;
            this.InfoButton.Click += new System.EventHandler(this.InfoButton_Click);
            // 
            // IndexBox
            // 
            this.IndexBox.Location = new System.Drawing.Point(31, 54);
            this.IndexBox.Name = "IndexBox";
            this.IndexBox.Size = new System.Drawing.Size(100, 20);
            this.IndexBox.TabIndex = 17;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Индекс";
            // 
            // NameBox
            // 
            this.NameBox.Location = new System.Drawing.Point(147, 54);
            this.NameBox.Name = "NameBox";
            this.NameBox.Size = new System.Drawing.Size(100, 20);
            this.NameBox.TabIndex = 15;
            // 
            // CostBox
            // 
            this.CostBox.Location = new System.Drawing.Point(368, 54);
            this.CostBox.Name = "CostBox";
            this.CostBox.Size = new System.Drawing.Size(100, 20);
            this.CostBox.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(365, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Цена";
            // 
            // AmountBox
            // 
            this.AmountBox.Location = new System.Drawing.Point(262, 54);
            this.AmountBox.Name = "AmountBox";
            this.AmountBox.Size = new System.Drawing.Size(100, 20);
            this.AmountBox.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(259, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Количество";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(144, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 10;
            this.label1.Text = "Наименование";
            // 
            // NameSearchButton
            // 
            this.NameSearchButton.Location = new System.Drawing.Point(337, 82);
            this.NameSearchButton.Name = "NameSearchButton";
            this.NameSearchButton.Size = new System.Drawing.Size(147, 43);
            this.NameSearchButton.TabIndex = 18;
            this.NameSearchButton.Text = "Получить информацию по имени";
            this.NameSearchButton.UseVisualStyleBackColor = true;
            this.NameSearchButton.Click += new System.EventHandler(this.NameSearchButton_Click);
            // 
            // grid
            // 
            this.grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grid.Location = new System.Drawing.Point(31, 235);
            this.grid.Name = "grid";
            this.grid.Size = new System.Drawing.Size(453, 197);
            this.grid.TabIndex = 19;
            // 
            // GenTableButton
            // 
            this.GenTableButton.Location = new System.Drawing.Point(31, 133);
            this.GenTableButton.Name = "GenTableButton";
            this.GenTableButton.Size = new System.Drawing.Size(147, 96);
            this.GenTableButton.TabIndex = 20;
            this.GenTableButton.Text = "Заполнить таблицу";
            this.GenTableButton.UseVisualStyleBackColor = true;
            this.GenTableButton.Click += new System.EventHandler(this.GenTableButton_Click);
            // 
            // AmountSortA
            // 
            this.AmountSortA.Location = new System.Drawing.Point(283, 133);
            this.AmountSortA.Name = "AmountSortA";
            this.AmountSortA.Size = new System.Drawing.Size(95, 45);
            this.AmountSortA.TabIndex = 22;
            this.AmountSortA.Text = "По количеству (Возрастание)";
            this.AmountSortA.UseVisualStyleBackColor = true;
            this.AmountSortA.Click += new System.EventHandler(this.AmountSortA_Click);
            // 
            // NameSortA
            // 
            this.NameSortA.Location = new System.Drawing.Point(184, 133);
            this.NameSortA.Name = "NameSortA";
            this.NameSortA.Size = new System.Drawing.Size(95, 45);
            this.NameSortA.TabIndex = 23;
            this.NameSortA.Text = "По имени (Возрастание)";
            this.NameSortA.UseVisualStyleBackColor = true;
            this.NameSortA.Click += new System.EventHandler(this.NameSortA_Click);
            // 
            // CostSortA
            // 
            this.CostSortA.Location = new System.Drawing.Point(384, 133);
            this.CostSortA.Name = "CostSortA";
            this.CostSortA.Size = new System.Drawing.Size(95, 45);
            this.CostSortA.TabIndex = 24;
            this.CostSortA.Text = "По цене (Возрастание)";
            this.CostSortA.UseVisualStyleBackColor = true;
            this.CostSortA.Click += new System.EventHandler(this.CostSortA_Click);
            // 
            // NameSortB
            // 
            this.NameSortB.Location = new System.Drawing.Point(184, 184);
            this.NameSortB.Name = "NameSortB";
            this.NameSortB.Size = new System.Drawing.Size(95, 45);
            this.NameSortB.TabIndex = 25;
            this.NameSortB.Text = "По имени (Убывание)";
            this.NameSortB.UseVisualStyleBackColor = true;
            this.NameSortB.Click += new System.EventHandler(this.NameSortB_Click);
            // 
            // AmountSortD
            // 
            this.AmountSortD.Location = new System.Drawing.Point(283, 184);
            this.AmountSortD.Name = "AmountSortD";
            this.AmountSortD.Size = new System.Drawing.Size(95, 45);
            this.AmountSortD.TabIndex = 26;
            this.AmountSortD.Text = "По количеству (Убывание)";
            this.AmountSortD.UseVisualStyleBackColor = true;
            this.AmountSortD.Click += new System.EventHandler(this.AmountSortD_Click);
            // 
            // CostSortD
            // 
            this.CostSortD.Location = new System.Drawing.Point(384, 184);
            this.CostSortD.Name = "CostSortD";
            this.CostSortD.Size = new System.Drawing.Size(95, 45);
            this.CostSortD.TabIndex = 27;
            this.CostSortD.Text = "По цене (Убывание)";
            this.CostSortD.UseVisualStyleBackColor = true;
            this.CostSortD.Click += new System.EventHandler(this.CostSortD_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 511);
            this.Controls.Add(this.CostSortD);
            this.Controls.Add(this.AmountSortD);
            this.Controls.Add(this.NameSortB);
            this.Controls.Add(this.CostSortA);
            this.Controls.Add(this.NameSortA);
            this.Controls.Add(this.AmountSortA);
            this.Controls.Add(this.GenTableButton);
            this.Controls.Add(this.grid);
            this.Controls.Add(this.NameSearchButton);
            this.Controls.Add(this.IndexBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.NameBox);
            this.Controls.Add(this.CostBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.AmountBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.InfoButton);
            this.Controls.Add(this.ChangeItemButton);
            this.Name = "MainForm";
            this.Text = "Работа со складом";
            ((System.ComponentModel.ISupportInitialize)(this.grid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ChangeItemButton;
        private System.Windows.Forms.Button InfoButton;
        private System.Windows.Forms.TextBox IndexBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox NameBox;
        private System.Windows.Forms.TextBox CostBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox AmountBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button NameSearchButton;
        private System.Windows.Forms.DataGridView grid;
        private System.Windows.Forms.Button GenTableButton;
        private System.Windows.Forms.Button AmountSortA;
        private System.Windows.Forms.Button NameSortA;
        private System.Windows.Forms.Button CostSortA;
        private System.Windows.Forms.Button NameSortB;
        private System.Windows.Forms.Button AmountSortD;
        private System.Windows.Forms.Button CostSortD;
    }
}


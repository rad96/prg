﻿namespace Task3
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.richTextBoxOrigenalText = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.richTextBoxChangedText = new System.Windows.Forms.RichTextBox();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.SBButton = new System.Windows.Forms.Button();
            this.StringButton = new System.Windows.Forms.Button();
            this.CharButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.richTextBoxOrigenalText);
            this.groupBox1.Location = new System.Drawing.Point(23, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(463, 398);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Оригинальный текст";
            // 
            // richTextBoxOrigenalText
            // 
            this.richTextBoxOrigenalText.Location = new System.Drawing.Point(6, 19);
            this.richTextBoxOrigenalText.Name = "richTextBoxOrigenalText";
            this.richTextBoxOrigenalText.Size = new System.Drawing.Size(451, 369);
            this.richTextBoxOrigenalText.TabIndex = 0;
            this.richTextBoxOrigenalText.Text = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.richTextBoxChangedText);
            this.groupBox2.Location = new System.Drawing.Point(565, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(452, 398);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Очищенный текст";
            // 
            // richTextBoxChangedText
            // 
            this.richTextBoxChangedText.Location = new System.Drawing.Point(7, 19);
            this.richTextBoxChangedText.Name = "richTextBoxChangedText";
            this.richTextBoxChangedText.Size = new System.Drawing.Size(445, 369);
            this.richTextBoxChangedText.TabIndex = 0;
            this.richTextBoxChangedText.Text = "";
            // 
            // buttonLoad
            // 
            this.buttonLoad.Location = new System.Drawing.Point(255, 432);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(122, 35);
            this.buttonLoad.TabIndex = 5;
            this.buttonLoad.Text = "Загрузить файл";
            this.buttonLoad.UseVisualStyleBackColor = true;
            // 
            // SBButton
            // 
            this.SBButton.Location = new System.Drawing.Point(404, 432);
            this.SBButton.Name = "SBButton";
            this.SBButton.Size = new System.Drawing.Size(105, 35);
            this.SBButton.TabIndex = 4;
            this.SBButton.Text = "Изменить (SB)";
            this.SBButton.UseVisualStyleBackColor = true;
            this.SBButton.Click += new System.EventHandler(this.SBButton_Click);
            // 
            // StringButton
            // 
            this.StringButton.Location = new System.Drawing.Point(527, 432);
            this.StringButton.Name = "StringButton";
            this.StringButton.Size = new System.Drawing.Size(113, 35);
            this.StringButton.TabIndex = 6;
            this.StringButton.Text = "Изменить (String)";
            this.StringButton.UseVisualStyleBackColor = true;
            this.StringButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // CharButton
            // 
            this.CharButton.Location = new System.Drawing.Point(658, 432);
            this.CharButton.Name = "CharButton";
            this.CharButton.Size = new System.Drawing.Size(113, 35);
            this.CharButton.TabIndex = 7;
            this.CharButton.Text = "Изменить (Char)";
            this.CharButton.UseVisualStyleBackColor = true;
            this.CharButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1032, 479);
            this.Controls.Add(this.CharButton);
            this.Controls.Add(this.StringButton);
            this.Controls.Add(this.buttonLoad);
            this.Controls.Add(this.SBButton);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RichTextBox richTextBoxOrigenalText;
        private System.Windows.Forms.RichTextBox richTextBoxChangedText;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Button SBButton;
        private System.Windows.Forms.Button StringButton;
        private System.Windows.Forms.Button CharButton;
    }
}


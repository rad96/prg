﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            richTextBoxOrigenalText.Clear();
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = "c:\\";
            openFileDialog.Filter = "cs files (*.cs)|*.cs|All files (*.*)|*.*";
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                richTextBoxOrigenalText.Text = File.ReadAllText(openFileDialog.FileName);
            }
        }

        private void SBButton_Click(object sender, EventArgs e)
        {
            Delete(new StringBuilder(richTextBoxOrigenalText.Text));
        }       

        private void button1_Click(object sender, EventArgs e)
        {
            DeleteWithString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            char[] chars = richTextBoxOrigenalText.Text.ToCharArray();
            Delete(chars);
        }

        public void Delete(char[] chars)
        {

            for (int i = 0; i < chars.Length; i++)
            {
                if (chars[i] == '/' && i + 1 < chars.Length && chars[i + 1] == '/')
                {
                    for (int j = i; j < chars.Length; j++)
                    {
                        if (chars[j] != '\n')
                        {
                            chars[j] = ' ';
                        }
                        else break;
                    }
                }
                else if (chars[i] == '/' && chars[i + 1] == '*')
                {
                    chars[i] = ' ';
                    chars[i + 1] = ' ';
                    for (int j = i + 2; j < chars.Length; j++)
                    {
                        if (chars[j] != '*' & chars[j + 1] != '/')
                        {
                            chars[j] = ' ';
                        }
                        else
                        {
                            chars[j] = ' ';
                            chars[j + 1] = ' ';
                            break;
                        }
                    }
                }
            }
            richTextBoxChangedText.Text = new string(chars);
        }

        public void Delete(StringBuilder text)
        {
            StringBuilder t = text;
            for (int i = 0; i < t.Length; i++)
            {
                if (t[i] == '/' && i + 1 < t.Length && t[i + 1] == '/')
                {
                    for (int j = i; j < t.Length; j++)
                    {
                        if (t[j] != '\n')
                        {
                            t[j] = ' ';
                        }
                        else break;
                    }
                }
                else if (t[i] == '/' && t[i + 1] == '*')
                {
                    t[i] = ' ';
                    t[i + 1] = ' ';
                    for (int j = i + 2; j < t.Length; j++)
                    {
                        if (t[j] != '*' & t[j + 1] != '/')
                        {
                            t[j] = ' ';
                        }
                        else
                        {
                            t[j] = ' ';
                            t[j + 1] = ' ';
                            break;
                        }
                    }
                }
            }
            richTextBoxChangedText.Text = t.ToString();
        }

        public void DeleteWithString()
        {
            string[] s = richTextBoxOrigenalText.Lines;
            //проход по строкам
            for (int i = 0; i < s.Length; i++)
            {
                string str = s[i];
                //проход по символам строки
                for (int j = 0; j < str.Length; j++)
                {
                    if (str[j] == '/')
                    {
                        if (j + 1 <= str.Length && str[j + 1] == '/')
                        {
                            s[i] = str.Remove(j-1);
                        }
                        else if (j + 1 <= str.Length && str[j + 1] == '*')
                        {
                            int countChars = 2;
                            bool isFind = false;
                            for (int k = j + 1; k < str.Length; k++)
                            {
                                countChars++;
                                if (str[k] == '*' && str[k + 1] == '/')
                                {
                                    isFind = true;
                                    s[i] = str.Remove(j, countChars);
                                }
                                if (!isFind)
                                {
                                    s[i] = str.Remove(j);
                                }
                            }
                        }
                    }
                }
            }
            richTextBoxChangedText.Lines = s;

        }
    }
}
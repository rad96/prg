﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_8._1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            int n = Convert.ToInt32(nBox.Text);
            arrayGrid.ColumnCount = 2 * n + 1;
            arrayGrid.RowCount = 2 * n + 1;
            int maxcount = n * n;
            int[,] msv = new int[2 * n + 1, 2 * n + 1];
            int i = msv.GetLength(0) / 2;
            int j = msv.GetLength(1) / 2;
            int value = 0;
            int step = 0;
            int side = 0;
            msv[i, j] = value;
            while (i <= msv.GetLength(0) || j <= msv.GetLength(1))
            {
                int temp;
                switch (side)
                {
                    case 1:
                        step++;
                        temp = j;
                        for (int t = 0; t < step; t++)
                            try { msv[i, j--] = value++; }
                            catch { value--; }
                        side = 2;
                        break;
                    case 2:
                        
                        temp = i;
                        for (int t = 0; t < step; t++)
                            try { msv[i--, j] = value++; }
                            catch { value--; }
                        side = 3;
                        break;
                    case 3:
                        step++;
                        temp = j;
                        for (int t = 0; t < step; t++)
                            try { msv[i, j++] = value++; }
                            catch { value--; }
                        side = 0;
                        break;
                    case 0:
                        
                        temp = i;
                        for (int t = 0; t < step; t++)
                            try { msv[i++, j] = value++; }
                            catch { value--; }
                        side = 1;
                        break;
                }
            }
            for (int k = 0; k < msv.GetLength(0); k++)
            {
                for (int l = 0; l < msv.GetLength(1); l++)
                {
                    arrayGrid[k, l].Value = msv[k, l];
                }
            }
        }
    }
}

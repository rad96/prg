using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace ConsoleApplication2
{
    class Books
    {
        string text;
        public enum title { classic, stihi, poem, fant };
        public title en;
        Books(string s)
        {
            text = s;
        }
        public void analysis()
        {
            switch (en)
            {
                case title.classic:
                    Console.WriteLine(text + ": классическая проза");
                    break;
                case title.stihi:
                    Console.WriteLine(text + ": стихи");
                    break;
                case title.poem:
                    Console.WriteLine(text + ": поэмы");
                    break;
                case title.fant:
                    Console.WriteLine(text + ": фантастика");
                    break;
                default:
                    Console.WriteLine("Автор неизвестен");
                    break;
            }
        }
        class Program
        {
            static void Main()
            {
                int i = 0;
                int b = 1;
                int t = 2;
                int d = 3;
                Books[] questions = new Books[4];
                questions[i] = new Books("Достоевский");
                questions[i].en = title.classic;
                questions[b] = new Books("Пушкин");
                questions[b].en = title.stihi;
                questions[t] = new Books("Шекспир");
                questions[t].en = title.poem;
                questions[d] = new Books("Гариссон");
                questions[d].en = title.fant;

                foreach (Books p in questions)
                {
                    p.analysis();
                }

                Console.ReadLine();
            }
        }
    }
}

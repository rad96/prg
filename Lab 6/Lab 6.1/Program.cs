﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_6._1
{
    class Programm
    {
        static void Main(string[] args)
        {
            Place plc = new Place();
            input(plc);
            plc.getmsW(plc);
            plc.getmsL(plc);
            output(plc);
            plc.choise(plc);
            plc.getmsW(plc);
            plc.getmsL(plc);
            output(plc);
            Console.ReadLine();
        }
        static void input(Place plc) 
        {
            plc.width = plc.insW();
            plc.longitude = plc.insL();
            plc.height = plc.insH();
        }
        static void output(Place plc)
        {
            Console.WriteLine("{1}°{2}'{3}\"{0}", plc.W, plc.degW, plc.minW, plc.secW);
            Console.WriteLine("{1}°{2}'{3}\"{0}", plc.L, plc.degL, plc.minL, plc.secL);
            Console.WriteLine("{0}", plc.height);
        }
    }
    public class Place
    {
        //Основные переменные
        public double degrees;
        public double width;
        public double longitude;
        public double height;        
        //Переменные широты
        public int degW;
        public int minW;
        public int secW;
        public char W;
        //Переменные долготы
        public int degL;
        public int minL;
        public int secL;
        public char L;
        public void choise(Place m)
        {
            Console.WriteLine("Вы желаете что-то изменить? (Ш, Д, В, Н):");
            string s = Convert.ToString(Console.ReadLine());
            if (s.ToLower() == "ш" | s.ToLower() == "широту") 
            {
                changeW(m);
            }
            else
            {
                if (s.ToLower() == "д" | s.ToLower() == "долготу") 
                {
                    changeL(m); 
                }
                else
                {
                    if (s.ToLower() == "в" | s.ToLower() == "высоту") 
                    {
                        changeH(m); 
                    } 
                }
            }
            Console.WriteLine("Вы желаете изменить еще что-то? (да/нет)");
            s = Convert.ToString(Console.ReadLine());
            if (s.ToLower() == "да") choise(m); else Console.WriteLine("Итоговые параметры объекта:");
        }
        public void changeW(Place m)
        {
            Console.Write("Введите число, на которое требуется изменить широту: ");
            double change = Convert.ToDouble(Console.ReadLine());
            if (m.width + change > 90 | m.width + change < -90)
            {
                Console.WriteLine("Значение выйдет за допустимые пределы при изменении, повторите ввод");
                changeW(m);
            }
            else
            {
                m.width += change;
            }
        }
        public void changeL(Place m)
        {
            Console.Write("Введите число, на которое требуется изменить долготу: ");
            double change = Convert.ToDouble(Console.ReadLine());
            if (m.longitude + change > 180 | m.longitude + change < -180)
            {
                Console.WriteLine("Значение выйдет за допустимые пределы при изменении, повторите ввод");
                changeL(m);
            }
            else
            {
                m.longitude += change;
            }
        }
        public void changeH(Place m)
        {
            Console.Write("Введите число, на которое требуется изменить высоту: ");
            double change = Convert.ToDouble(Console.ReadLine());
            if (m.height + change > 10000 | m.height + change < -10000)
            {
                Console.WriteLine("Значение выйдет за допустимые пределы при изменении, повторите ввод");
                changeH(m);
            }
            else
            {
                m.height += change;
            }
        }
        public void getmsW(Place m) //Преобразование градусов широты в градусы, минуты, секунды
        {
            m.secW = (int)Math.Round(m.width * 3600);
            m.degW = m.secW / 3600;
            m.secW = Math.Abs(m.secW % 3600);
            m.minW = m.secW / 60;
            m.secW %= 60;
            charW(m);
        }
        public void getmsL(Place m) //Преобразование градусов долготы в градусы, минуты, секунды
        {
            m.secL = (int)Math.Round(m.longitude * 3600);
            m.degL = m.secL / 3600;
            m.secL = Math.Abs(m.secL % 3600);
            m.minL = m.secL / 60;
            m.secL %= 60;
            charL(m);
        }
        static void charW(Place m)
        {
            if (m.width >= 0) m.W = 'N'; else m.W = 'S';
        }
        static void charL(Place m)
        {
            if (m.longitude >= 0) m.L = 'E'; else m.L = 'W';
        }
        //3 ввода данных (Широта, долгота, высота)
        public double insW()
        {
            Console.Write("Введите широту (в градусах): "); double x = Convert.ToDouble(Console.ReadLine());
            if (x > 90 | x < -90)
            {
                Console.WriteLine("Вы ввели недопустимое значение, повторите ввод");
                x = insW();
            }
            return x;
        }
        public double insL()
        {
            Console.Write("Введите долготу (в градусах): "); double x = Convert.ToDouble(Console.ReadLine());
            if (x > 180 | x < -180)
            {
                Console.WriteLine("Вы ввели недопустимое значение, повторите ввод");
                x = insL();
            }
            return x;
        }
        public double insH()
        {
            Console.Write("Введите высоту (в метрах): ");  double x = Convert.ToDouble(Console.ReadLine());
            if (x > 10000 | x < -10000)
            {
                Console.WriteLine("Вы ввели недопустимое значение, повторите ввод");
                x = insH();
            }
            return x;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Lab_6._3
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader file = new StreamReader("input.txt");
            int numofpupils = Convert.ToInt32(file.ReadLine());
            pupil[] a = new pupil[numofpupils];
            for (int i = 0; i < numofpupils; i++)
            {
                a[i] = new pupil();
            }
            for (int i = 0; i < numofpupils; i++)
            {
                parse(a, i, file);
                a[i].mathave(a[i], a[i].math, a[i].phis, a[i].inf);
            }
            for (int i = 0; i < numofpupils; i++)
            {
                Console.WriteLine("{0} {1} {2} {3} {4} Средняя оценка: {5}", a[i].sname,a[i].fname, a[i].math, a[i].phis, a[i].inf, a[i].average);
            }
            Console.ReadLine();
            file.Close();
        }
        static void parse(pupil[] a, int i, StreamReader file)
        {
            string onep = file.ReadLine();
            string[] split = onep.Split(' ');
            a[i].sname = split[0];
            a[i].fname = split[1];
            a[i].math = Convert.ToInt32(split[2]);
            a[i].phis = Convert.ToInt32(split[3]);
            a[i].inf = Convert.ToInt32(split[4]);
        }
        public class pupil
        {
            public string fname; //Имя
            public string sname; //Фамилия
            public int math;
            public int phis;
            public int inf;
            public double average;
            public void mathave(pupil a, int math, int phis, int inf)
            {
                a.average = (a.math + a.phis + a.inf)/3;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_2._2
{
    class Program
    {
        static void Main(string[] args)
        {
            int x;
            Console.Write("Введите x: "); x = Convert.ToInt32(Console.ReadLine());
            int sum = 0;
            while (x != 0)
            {
                sum += x % 10;
                x /= 10;
            }
            Console.WriteLine("Сумма цифр числа x: {0}", sum);
            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Task1
{
    public partial class Form1 : Form
    {
        Uni CandidateU;
        Abitur CandidateA;
        UniProperties[] unis;
        UniProperties[] findUnis;
        UniverName[] nameGirls;
        AbiturProperties[] findMan;
        AbiturProperties[] man;
        AbiturNames[] nameMan;


        public Form1()
        {
            InitializeComponent();

            CandidateU = new Uni();
            CandidateU.formWifes();
            unis = CandidateU.getCandidate();
            nameGirls = CandidateU.getNameCandidate();
            findMan = CandidateU.getFindFirm();

            CandidateA = new Abitur();
            CandidateA.createHusbands();
            man = CandidateA.getHusbandProperties();
            nameMan = CandidateA.getHusbandNames();
            findUnis = CandidateA.getFirmFindProperties();
        }
        private void cBoxType_SelectedIndexChanged(object sender, EventArgs e)
        {
            cBoxOfCandidateSName.Items.Clear();
            cBoxOfCandidateSName.Text = "";
            lBoxOutData.Items.Clear();
            if (cBoxTypeOfSex.SelectedIndex == 0) // возвращает или задает индекс
            {
                
                for (int i = 0; i < nameGirls.Length; i++)
                    cBoxOfCandidateSName.Items.Add("Невеста " + i + " - " + nameGirls[i]);
            }
            else
            {
                
                for (int i = 0; i < nameMan.Length; i++)
                    cBoxOfCandidateSName.Items.Add("Жених " + i + " - " + nameMan[i]);
            }
        }

        private void cBoxObject_SelectedIndexChanged(object sender, EventArgs e)
        {
            lBoxOutData.Items.Clear();
            if (cBoxTypeOfSex.SelectedIndex == 0)
            {
                lblStats.Text = unis[cBoxOfCandidateSName.SelectedIndex].ToString();
                lblFindStats.Text = findMan[cBoxOfCandidateSName.SelectedIndex].ToString();
                string[] result = CandidateU.getFindHusbands(man, findMan[cBoxOfCandidateSName.SelectedIndex], nameMan);
                for (int i = 0; i < result.Length; i++)
                    if (result[i]!=null)
                        lBoxOutData.Items.Add(result[i]);
            }
            else
            {
                lblStats.Text = man[cBoxOfCandidateSName.SelectedIndex].ToString();
                lblFindStats.Text = findUnis[cBoxOfCandidateSName.SelectedIndex].ToString();
                string[] result = CandidateA.getFindWifes(unis, findUnis[cBoxOfCandidateSName.SelectedIndex], nameGirls);
                for (int i = 0; i < result.Length; i++)
                    if (result[i] != null)
                        lBoxOutData.Items.Add(result[i]);
            }
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            CandidateU.formWifes();
            unis = CandidateU.getCandidate();
            nameGirls = CandidateU.getNameCandidate();
            findMan = CandidateU.getFindFirm();

            CandidateA.createHusbands();
            man = CandidateA.getHusbandProperties();
            nameMan = CandidateA.getHusbandNames();
            findUnis = CandidateA.getFirmFindProperties();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void lBoxOutData_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {

        }

        

        
    }
}

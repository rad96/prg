﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task1
{
    public enum UniverName //имена ж
    {
        ПГУТИ = 0, СамГТУ = 1, СГАУ = 2, СГАСУ = 3, СамГУ = 4, СГАКИ = 5
    }
    [Flags]
    public enum UniProperties //свойства ж
    {
        Большой = 1, Бюджетный = 2, Качественный = 4, Государственный = 8, Престижный = 20
    }
    class Uni
    {
        int number;
        UniProperties[] Univeres;
        UniverName [] nameUni;
        AbiturProperties[] findWifes;
        string[] strWifes;
        Random rand;

        public Uni()
        {
            number = 6;
            Univeres = new UniProperties[number];
            nameUni = new UniverName[number];
            strWifes = new string[number];
            findWifes = new AbiturProperties[number];
            rand = new Random();
        }

        public Uni(int number)
        {
            this.number = number;
            Univeres = new UniProperties[number];
            nameUni = new UniverName[number];
            strWifes = new string[number];
            findWifes = new AbiturProperties[number];
            rand = new Random();
        }

        public Uni(UniProperties[] Wife)
        {
            number = Wife.Length;
            this.Univeres = Wife;
            nameUni = new UniverName[number];
            strWifes = new string[number];
            findWifes = new AbiturProperties[number];
            rand = new Random();
        }

        public AbiturProperties[] getFindFirm()
        {
            return findWifes;
        }

        public UniProperties[] getCandidate()
        {
            return Univeres;
        }

        public UniverName[] getNameCandidate()
        {
            return nameUni;
        }

        public void formWifes()
        {
            int properities = 5;
            int p = 0, currentProps = 0, currentPropsHusbands = 0;
            for (int i = 0; i < number; i++)
            {
                currentProps = 0; currentPropsHusbands = 0;
                for (int j = 0; j < properities; j++)
                {
                    p = rand.Next(3);
                    if (p == 1)
                        currentProps += (int)Math.Pow(2, j);
                    if (p == 2)
                        currentPropsHusbands += (int)Math.Pow(2, j);
                }
                Univeres[i] = (UniProperties)currentProps;
                findWifes[i] = (AbiturProperties)currentPropsHusbands;
                nameUni[i] = (UniverName)rand.Next(13);
            }
        }

        public string[] getFindHusbands(AbiturProperties[] Husbands, AbiturProperties findProperies, AbiturProperties[] nameHusbands)
        {
            int[] numGoodPropeties = new int[Husbands.Length];
            int count = 0;
            string[] result = new string[Husbands.Length];
            for (int i = 0; i < Husbands.Length; i++)
            {
                if (((Husbands[i] & findProperies) > 0)||(findProperies==0))
                {
                    result[count] = "Жених № " + i + " " + nameHusbands[i].ToString() + " \n" + Husbands[i].ToString();
                    numGoodPropeties[count] = (int)(Husbands[i] & findProperies);
                    count++;
                }
            }
            sortHusbands(result, numGoodPropeties);
            return result;

        }

        void sortHusbands(string[] Husbands, int[] numGoodProperties)
        {
            string bufFirm;
            int bufGoodProperties;
            for (int j = 0; j < Husbands.Length - 1; j++)
            {
                for (int i = 0; i < Husbands.Length - 1; i++)
                {
        
                        if (numGoodProperties[i] < numGoodProperties[i + 1])
                        {
                            bufFirm = Husbands[i + 1];
                            bufGoodProperties = numGoodProperties[i + 1];
                            Husbands[i + 1] = Husbands[i];
                            numGoodProperties[i + 1] = numGoodProperties[i];
                            Husbands[i] = bufFirm;
                            numGoodProperties[i] = bufGoodProperties;

                        }
                }
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Task1
{
    public enum AbiturNames //имена м
    {
        Владислав = 0, Сергей = 1, Дмитрий = 2, Владимир = 3, Александр = 4, Николай = 5
    }
    [Flags]
    public enum AbiturProperties // свойства м
    {
        Ответственный = 1, Прилежный = 2, Смекалистый = 4, Упорный = 8, Трудолюбивый = 16, Умный = 20
    }
    class Abitur
    {
        int number;
        AbiturProperties[] Abiturients; // объект свойств м
        AbiturNames[] nameHusbands; // объект имен м
        UniProperties[] findHusbands; // объект свойств ж
        string[] strHusbands; // свойства м в стр
        Random rand;
        // конструкторы
        public Abitur()
        {
            number = 10;
            Abiturients = new AbiturProperties[number];
            nameHusbands = new AbiturNames[number];
            strHusbands = new string[number];
            findHusbands = new UniProperties[number];
            rand = new Random();
        }

        public Abitur(int number)
        {
            this.number = number;
            Abiturients = new AbiturProperties[number];
            nameHusbands = new AbiturNames[number];
            strHusbands = new string[number];
            findHusbands = new UniProperties[number];
            rand = new Random();
        }

        public Abitur(AbiturProperties[] Husband)
        {
            number = Husband.Length; // кол-во св-в
            this.Abiturients = Husband;
            nameHusbands = new AbiturNames[number];
            strHusbands = new string[number];
            findHusbands = new UniProperties[number];
            rand = new Random();
        }

        public AbiturNames[] getHusbandNames()
        {
            return nameHusbands;
        }

        public AbiturProperties[] getHusbandProperties()
        {
            return Abiturients;
        }

        public UniProperties[] getFirmFindProperties()
        {
            return findHusbands;
        }

        public void createHusbands()
        {
            int properties = 5;
            int p = 0, currentPropsWifes, currentProps;
            for (int i = 0; i < number; i++)
            {
                currentProps = 0; currentPropsWifes = 0;
                for (int j = 0; j < properties; j++)
                {
                    p = rand.Next(3);
                    if (p == 1)
                        currentProps += (int)Math.Pow(2, j);
                    if (p == 2)
                        currentPropsWifes += (int)Math.Pow(2, j);
                }
                Abiturients[i] = (AbiturProperties)currentProps;
                findHusbands[i] = (UniProperties)currentPropsWifes;
                nameHusbands[i] = (AbiturNames)rand.Next(6);
            }
        }

        public string[] getFindWifes(UniProperties[] Wifes, UniProperties findProperies, UniverName[] nameWifes)
        {
            int[] numGoodPropeties = new int[Wifes.Length];
            int count = 0;
            string[] result = new string[Wifes.Length];
            for (int i = 0; i < Wifes.Length; i++)
            {
                if (((Wifes[i] & findProperies) > 0)||(findProperies==0))
                {
                    result[count] = "Невеста № " + i + " " + nameWifes[i].ToString() + " \n" + Wifes[i].ToString();
                    numGoodPropeties[count] = (int)(Wifes[i] & findProperies);
                    count++;
                }
            }
            sortWifes(result, numGoodPropeties);
            return result;
        }

        void sortWifes(string[] Wifes, int[] numGoodProperties)
        {
            string bufCandidate;
            int bufGoodProperties;
            for (int j = 0; j < Wifes.Length - 1; j++)
            {
                for (int i = 0; i < Wifes.Length - 1; i++)
                {
                        if (numGoodProperties[i] < numGoodProperties[i + 1])
                        {
                            bufCandidate = Wifes[i + 1];
                            bufGoodProperties = numGoodProperties[i + 1];
                            Wifes[i + 1] = Wifes[i];
                            numGoodProperties[i + 1] = numGoodProperties[i];
                            Wifes[i] = bufCandidate;
                            numGoodProperties[i] = bufGoodProperties;
                        }
                }
            }
        }

    }
}
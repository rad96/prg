﻿using MathTools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            grid.ColumnCount = 5;
            grid.Columns[0].HeaderText = "Значение x";
            grid.Columns[1].HeaderText = "Math.ArcSin";
            grid.Columns[2].HeaderText = "MyArcsin";
            grid.Columns[3].HeaderText = "Разница";
            grid.Columns[4].HeaderText = "Сумма";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Double x1 = Convert.ToDouble(textBox1.Text);
            Double x2 = Convert.ToDouble(textBox2.Text);
            Double dx = Convert.ToDouble(textBox3.Text);
            Double eps = Convert.ToDouble(textBox4.Text);
            int rowc = Convert.ToInt32(x2 / dx);
            grid.RowCount = rowc;
            int n = 0;
            do
            {
                grid[0, n].Value = x1;
                grid[1, n].Value = Math.Asin(x1);
                double[] temparray = MyMath.ArcSin(x1, eps);
                grid[2, n].Value = temparray[0];
                grid[3, n].Value = (double)grid[1, n].Value - (double)grid[2, n].Value;
                grid[4, n].Value = temparray[1];
                n++;
                x1 += dx;
            }
            while (x1 <= x2);
            
            int count = 1;
            //Double x = Convert.ToDouble(textBox1.Text);
            count = Convert.ToInt32(textBoxCount.Text);
            textBoxTimeMySin.Text =(MyMath.TimeValue.EvalTimeDToD(count,MyMath.OldArcSin, x1)).ToString();
            textBox5.Text = (MyMath.TimeValue.EvalTimeDToD(count, Math.Asin, x1)).ToString();

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

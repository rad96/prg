﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MathTools
{
    public class MyMath
    {
        const double TWOPI = 2 * Math.PI;
        const double EPS = 1E-9;

        public static double[] ArcSin(double x, double eps)
        {
            //Оптимизация - приведение к интервалу
            
            double res = x;
           
            double k = 1;
            double temp = Math.Pow(x, 3) / 6.0;
            double q = (2 * k - 1) * (2 * k - 1);
            double w = x * x;

            //Основные вычисления
            while (Math.Abs(temp) >= eps)
            {
                res += temp;
                k++;
                temp *= (q * w);
               
                temp /= 2 * k * (2 * k + 1);
                
            }
            double[] array = { res, k };
            return array;
        }

        public static double OldArcSin(double x)
        {

            x = x % TWOPI;

            double res = x;

            double k = 1;
            double temp = Math.Pow(x, 3) / 6.0;
            double q = (2 * k - 1) * (2 * k - 1);
            double w = x * x;

            while (Math.Abs(temp) > EPS)
            {
                res += temp;
                k++;
                temp *= (q * w);
                temp /= 2 * k * (2 * k + 1);
            }
            return res;
        }


        public class TimeValue
        {
            public delegate double DToD(double arg1);

            public static double EvalTimeDToD(int count, DToD fun, double x)
            {
                DateTime start, finish;
                double res = 0;
                start = DateTime.Now;
                for (int i = 1; i < count; i++)
                    fun(x);
                finish = DateTime.Now;
                res = (finish - start).Milliseconds;
                return res;
            }
        }
    }
}


